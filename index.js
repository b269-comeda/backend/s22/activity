/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

// 1. STARTS HERE

function registerUser(username){
	if (registeredUsers.includes(username)) {
		alert("Registration failed. Username already exists!");
	} else {
		registeredUsers.push(username);
		alert("Thank you for registering!");
	}
}

// registering a new user
registerUser("Jeremiah");

// logging the current registeredUsers array
console.log(registeredUsers);

// 1. ENDS HERE

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

// 2. STARTS HERE

function addFriend(foundUser) {
	if (registeredUsers.includes(foundUser)) {
		friendsList.push(foundUser);
		alert("You have added " + foundUser + " as a friend!");
	} else {
		alert("User not found.");
	}
}

// adding a registered user
addFriend("Macie West");

// logging the current friends list
console.log(friendsList);
// 2. ENDS HERE

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    
// 3. STARTS HERE
function displayFriends(){
	
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	} else {
		friendsList.forEach(function(friend){
			console.log(friend);
		})
	}
}
// invoking the current friends list one by one on our console
displayFriends();
// 3. ENDS HERE

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

// 4. STARTS HERE
function displayAmountOfFriends(){
	if (friendsList.length === 0){
		alert("You currently have 0 friends. Add one first.");
	} else {
		alert("You currently have " + friendsList.length + " friends.");
	}
}

displayAmountOfFriends();
// 4. ENDS HERE

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

// 5. STARTS HERE
function deleteFriend(friendFound) {
	if (friendsList.length === 0){
		alert("You currently have 0 friends. Add one first.");
	} else if(friendsList.includes(friendFound)) {
		let indexOfThatFriend = friendsList.indexOf(friendFound);
		friendsList.splice(indexOfThatFriend, 1);
	} else {
		friendsList.pop();
	}
}

// 5. ENDS HERE

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






